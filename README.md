# bigodon

Integrantes:
	Eric Ryuiti Shimizu
	Willians Oliveira
	Yuri Nagy 

Funcionalidades Faltantes:
	Recuperação de senha ainda não implementado.
	Não é possível cadastrar ou trocar foto do usuário.
	No "Menu" o histórico e agenda não foram implementados.
	A funcionalidade de agendar um horário por enquanto está só para consulta de horários disponíveis.
	A alteração de dados tanto de usuário quanto barbearia ainda não está dinâmica.
	Ao entrar na página "Minha Conta" os dados não são recuperados.
	Não está implementado a validação de email no cadastrar usuario.

Atividades de cada membro:
	Willians desenvolveu as telas de Splash, WelcomeScreen, login, Signgup, PasswordRecover, SideMenu, MyAccount e FavoritePage
	Eric desenvolveu as telas de HomePage, DetailPage, modelo e controlador de barbearia.
	Yuri desenvolveu modelo e controlador de usuário para cadastro/login e validações de login.
