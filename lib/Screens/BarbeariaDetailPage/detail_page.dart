import 'package:bigodon/Screens/HomePage/components/barbeariasRow.dart';
import 'package:bigodon/models/barbearia.dart';
import 'package:bigodon/widgets/image_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import '../../controllers/barbearia_controller.dart';
import '../../models/barbearia.dart';

class DetailPage extends StatefulWidget {
  final Barbearia barbearia;

  DetailPage(this.barbearia);

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {
    final controller = Get.put(BarbeariaController());
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        shape: ContinuousRectangleBorder(
          borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(30),
            bottomRight: Radius.circular(30),
          ),
        ),
        title: Image.asset(
          "assets/images/bigodon_black.png",
          fit: BoxFit.contain,
          height: 40,
        ),
      ),
      body: Container(
        constraints: BoxConstraints.expand(),
        color: Colors.white,
        child: Stack(
          children: <Widget>[
            _getBackGround(),
            _getGradient(),
            _getContent(context, controller),

            // _getToolbar(context),
          ],
        ),
      ),
    );
  }

  // MOSTRAR O MAPA
  /*
  body: GetBuilder<BarbeariasController>(
    init: controllerMapa,
    builder: (value) => GoogleMap(
      MapType: MapType.normal,
      zoomControlsEnabled: true,
      initialCameraPosition: CameraPosition(
        target: controllerMapa.position,
        zoom: 13,
      ),
      onMapCreated: contollerMapa.onMapCreated,
      myLocationEnabled: true,
    ),
  );
  */
  Container _mostraMapa(controller) {
    return Container(
      child: GoogleMap(
        mapType: MapType.normal,
        zoomControlsEnabled: true,
        initialCameraPosition: CameraPosition(
          target: controller.position,
          zoom: 13,
        ),
        onMapCreated: controller.onMapCreated,
        myLocationEnabled: true,
      ),
    );
  }

  Container _getBackGround() {
    return Container(
      child: Image.network(
        widget.barbearia.imagemFundo,
        fit: BoxFit.cover,
        height: 200,
      ),
      constraints: BoxConstraints.expand(height: 200),
    );
  }

  Container _getGradient() {
    return Container(
      margin: EdgeInsets.only(top: 100),
      height: 100,
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: <Color>[Color(0x00FFFFFF), Colors.white],
            stops: [0.0, 0.9],
            begin: FractionalOffset(0.0, 0.0),
            end: FractionalOffset(0.0, 1.0)),
      ),
    );
  }

  Widget _getContent(context, controller) {
    return Container(
      padding: EdgeInsets.fromLTRB(0.0, 50.0, 0.0, 0.0),
      child: Column(
        children: <Widget>[
          BarbeariaRow(barbearia: widget.barbearia, horizontal: false),
          DefaultTabController(
            length: 2,
            child: SizedBox(
              height: MediaQuery.of(context).size.height * 0.47,
              // padding: EdgeInsets.symmetric(horizontal: 32),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  TabBar(
                    tabs: [
                      Tab(
                        icon: Icon(Icons.watch_later),
                        text: 'Horários Disponíveis',
                      ),
                      Tab(
                        icon: Icon(Icons.image),
                        text: 'Galeria',
                      ),
                      // Tab(
                      //   icon: Icon(Icons.map_outlined),
                      //   text: 'Localizaçaõ',
                      // ),
                    ],
                    indicatorColor: Colors.red,
                  ),
                  Expanded(
                    child: TabBarView(
                      children: [
                        horarios(context),
                        imagens(context), // Text("data"),
                        // mapa(context, controller),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget horarios(context) {
    final qtd = widget.barbearia.horarios.length;

    return qtd == 0
        ? Container(
            child: Center(
              child: Text('Nenhum Horário Disponível :('),
            ),
          )
        : Container(
            padding: EdgeInsets.all(5),
            child: GridView.count(
              crossAxisCount: 4,
              crossAxisSpacing: 3,
              mainAxisSpacing: 3,
              children: List.generate(qtd, (index) {
                return GestureDetector(
                  child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      //  border: Border.all(color: Colors.red[700], width: 2),
                      color: Colors.green[400],
                      borderRadius: BorderRadius.circular(5),
                    ),
                    // padding: EdgeInsets.all(10),
                    child: Text(
                      widget.barbearia.horarios[index],
                      style: TextStyle(fontSize: 23, color: Colors.white
                          // fontWeight: FontWeight.w300
                          ),
                    ),
                    // color: Colors.grey[300],
                  ),
                  onTap: () {},
                );
              }),
            ),
          );
  }

  Widget imagens(context) {
    final qtd = widget.barbearia.galeria.length;

    return qtd == 0
        ? Container(
            child: Center(
              child: Text('Nenhuma imagem :('),
            ),
          )
        : Container(
            padding: EdgeInsets.all(5),
            child: GridView.count(
              crossAxisCount: 3,
              crossAxisSpacing: 2,
              mainAxisSpacing: 2,
              children: List.generate(qtd, (index) {
                return GestureDetector(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      image: DecorationImage(
                        image: NetworkImage(
                          widget.barbearia.galeria[index],
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                    // padding: EdgeInsets.all(2),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) {
                          return ImageScreen(
                            imageToFull: widget.barbearia.galeria[index],
                          );
                        },
                      ),
                    );
                  },
                );
              }),
            ),
          );
  }

  Widget mapa(context, controller) {
    return _mostraMapa(controller);
  }
}
