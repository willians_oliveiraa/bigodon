import 'package:bigodon/Screens/HomePage/components/card_barbearia.dart';
import 'package:bigodon/Screens/HomePage/components/foto_barbearia.dart';
import 'package:bigodon/controllers/home_controller.dart';
import 'package:bigodon/models/barbearia.dart';
import 'package:flutter/material.dart';

class FavoritePage extends StatefulWidget {
  FavoritePage({Key key}) : super(key: key);

  @override
  _FavoritePageState createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  var controller;

  @override
  void initState() {
    super.initState();
    controller = HomeController();
  }

  Widget build(BuildContext context) {
    var scaffold = Scaffold(
      appBar: AppBar(
        shape: ContinuousRectangleBorder(
          borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(30),
            bottomRight: Radius.circular(30),
          ),
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              "assets/images/bigodon_black.png",
              fit: BoxFit.contain,
              height: 40,
            ),
          ],
        ),
      ),
      body: Container(
        // margin: EdgeInsets.only(top: 10),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: Text(
                "Barbearias Favoritas",
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600),
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: controller.tabela.length,
                // ignore: missing_return
                itemBuilder: (BuildContext contexto, int i) {
                  final List<Barbearia> tabela = controller.tabela;
                  if (tabela[i].like) {
                    return Container(
                      margin: EdgeInsets.symmetric(
                        vertical: 8,
                        horizontal: 24,
                      ),
                      child: Stack(
                        children: <Widget>[
                          CardBarbearia(
                            barbearia: tabela[i],
                            horizontal: true,
                          ),
                          FotoBarbearia(
                            barber_image: tabela[i].logo,
                            horizontal: true,
                          ),
                        ],
                      ),
                    );
                  } else {
                    return Text("");
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
    return scaffold;
  }
}
