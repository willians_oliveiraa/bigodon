import 'package:bigodon/Screens/HomePage/components/card_barbearia.dart';
import 'package:bigodon/Screens/HomePage/components/foto_barbearia.dart';
import 'package:bigodon/models/barbearia.dart';
import 'package:flutter/material.dart';

class BarbeariaRow extends StatelessWidget {
  final Barbearia barbearia;
  final bool horizontal;

  const BarbeariaRow({Key key, this.barbearia, this.horizontal = true})
      : super(key: key);
  BarbeariaRow.vertical(this.barbearia) : horizontal = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        top: 16.0,
        bottom: 16.0,
        left: 30.0,
        right: 30.0,
      ),
      child: Stack(
        children: <Widget>[
          CardBarbearia(barbearia: barbearia, horizontal: horizontal),
          FotoBarbearia(
            barber_image: barbearia.logo,
            horizontal: horizontal,
          ),
        ],
      ),
    );
  }
}
