import 'package:bigodon/Screens/BarbeariaDetailPage/detail_page.dart';
import 'package:bigodon/Screens/HomePage/components/card_content_barbearia.dart';
import 'package:bigodon/controllers/barbearia_controller.dart';
import 'package:bigodon/models/barbearia.dart';
import 'package:flutter/material.dart';

class CardBarbearia extends StatelessWidget {
  final Barbearia barbearia;
  final bool horizontal;

  const CardBarbearia({Key key, this.barbearia, this.horizontal})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        child: CardContentBarbearia(
          barbearia: barbearia,
          horizontal: horizontal,
        ),
        height: horizontal ? 124.0 : 170,
        margin: horizontal
            ? EdgeInsets.only(left: 40.0)
            : EdgeInsets.only(top: 72.0),
        decoration: BoxDecoration(
          color: Colors.grey[600],
          shape: BoxShape.rectangle,
          border: Border.all(color: Colors.white, width: 2),
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black12,
              blurRadius: 10.0,
              offset: new Offset(0.0, 10.0),
            ),
          ],
        ),
      ),
      onTap: horizontal
          ? () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (_) {
                  return DetailPage(barbearia);
                }),
              );
            }
          : null,
    );
  }
}
