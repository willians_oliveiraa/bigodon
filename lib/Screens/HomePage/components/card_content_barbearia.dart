import 'package:bigodon/models/barbearia.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CardContentBarbearia extends StatefulWidget {
  final Barbearia barbearia;
  final bool horizontal;

  const CardContentBarbearia({Key key, this.barbearia, this.horizontal})
      : super(key: key);

  @override
  _CardContentBarbeariaState createState() => _CardContentBarbeariaState();
}

class _CardContentBarbeariaState extends State<CardContentBarbearia> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(
        widget.horizontal ? 70.0 : 16.0,
        widget.horizontal ? 16 : 10,
        20.0,
        16.0,
      ),
      constraints: BoxConstraints.expand(),
      child: Column(
        crossAxisAlignment: widget.horizontal
            ? CrossAxisAlignment.start
            : CrossAxisAlignment.center,
        mainAxisAlignment: widget.horizontal
            ? MainAxisAlignment.center
            : MainAxisAlignment.start,
        children: <Widget>[
          if (!widget.horizontal)
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.location_on_outlined,
                          color: Colors.white,
                          size: 17,
                        ),
                        Text(
                          widget.barbearia.distancia,
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  InkWell(
                    child: Icon(
                      widget.barbearia.like
                          ? Icons.favorite
                          : Icons.favorite_border,
                      color: Colors.red[400],
                    ),
                    onTap: _setlike,
                  ),
                ],
              ),
            ),
          if (!widget.horizontal)
            Container(
              height: 35,
            ),
          Text(
            widget.barbearia.nome,
            style: TextStyle(
              color: Colors.white,
              fontSize: widget.horizontal ? 23 : 28,
              fontWeight: FontWeight.w600,
            ),
          ),
          Text(
            widget.barbearia.descricao,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w400,
              fontSize: 15,
            ),
          ),
          Container(
            height: 5,
          ),
          if (!widget.horizontal)
            Text(
              widget.barbearia.telefone,
              style: TextStyle(color: Colors.white),
            ),
          if (widget.horizontal)
            Container(
              margin: EdgeInsets.symmetric(vertical: 5.0),
              height: 1.0,
              width: double.infinity,
              color: Colors.white,
            ),
          // Container(height: 13.0),
          if (widget.horizontal)
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.location_on_outlined,
                          color: Colors.white,
                          size: 17,
                        ),
                        Text(
                          widget.barbearia.distancia,
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  InkWell(
                    child: Icon(
                      widget.barbearia.like
                          ? Icons.favorite
                          : Icons.favorite_border,
                      color: Colors.red[400],
                    ),
                    onTap: _setlike,
                  ),
                ],
              ),
            ),
        ],
      ),
    );
  }

  void _setlike() {
    widget.barbearia.like = !widget.barbearia.like;
    setState(() {});
  }
}
