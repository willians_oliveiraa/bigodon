import 'package:bigodon/widgets/image_screen.dart';
import 'package:flutter/material.dart';

class FotoBarbearia extends StatelessWidget {
  final String barber_image;
  final bool horizontal;

  const FotoBarbearia({Key key, this.barber_image, this.horizontal})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.symmetric(vertical: horizontal ? 16.0 : 0.0),
        alignment:
            horizontal ? FractionalOffset.centerLeft : FractionalOffset.center,
        child: Container(
          width: horizontal ? 95 : 140,
          height: horizontal ? 95 : 140,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              image: NetworkImage(barber_image),
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (_) {
            return ImageScreen(
              imageToFull: barber_image,
            );
          }),
        );
      },
    );
  }
}
