import 'package:bigodon/Screens/side_menu.dart';
import 'package:bigodon/controllers/barbearia_controller.dart';
import 'package:bigodon/controllers/usuario_controller.dart';
import 'package:bigodon/models/barbearia.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../HomePage/components/card_barbearia.dart';
import '../HomePage/components/foto_barbearia.dart';
import '../map_screen.dart';

class HomePage extends StatefulWidget {
  final String title;

  HomePage({
    Key key,
    this.title = "Página Principal",
  }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final UsuarioController controller_user = Get.put(UsuarioController());
  final controller = Get.put(BarbeariaController());

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //final controllerMapa = Get.put(BarbeariasController);
    var scaffold = Scaffold(
      appBar: AppBar(
        shape: ContinuousRectangleBorder(
          borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(30),
            bottomRight: Radius.circular(30),
          ),
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              child: Image.asset(
                "assets/images/bigodon_black.png",
                fit: BoxFit.contain,
                height: 40,
              ),
              onTap: () {
                setState(() {});
              },
            ),
          ],
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.map),
            tooltip: 'Abrir Menu',
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return MapScreen();
                  },
                ),
              );
            },
          ),
        ],
      ),
      drawer: SideMenu(),
      body: Container(
        // margin: EdgeInsets.only(top: 10),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: Text(
                "Escolha uma Barbearia:",
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600),
              ),
            ),
            Obx(() {
              return Expanded(
                child: controller.tabela.length < 0
                    ? Center(child: Text('Sem conexão com internet!'))
                    : ListView.builder(
                        itemCount: controller.tabela.length,
                        itemBuilder: (BuildContext contexto, int i) {
                          RxList<dynamic> tabela = controller.tabela;
                          return Container(
                            margin: EdgeInsets.symmetric(
                              vertical: 8,
                              horizontal: 24,
                            ),
                            child: Stack(
                              children: <Widget>[
                                CardBarbearia(
                                  barbearia: tabela[i],
                                  horizontal: true,
                                ),
                                FotoBarbearia(
                                  barber_image: tabela[i].logo,
                                  horizontal: true,
                                ),
                              ],
                            ),
                          );
                        },
                      ),
              );
            })
          ],
        ),
      ),
    );
    return scaffold;
  }
}
