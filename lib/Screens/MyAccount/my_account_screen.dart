import 'dart:io';
import 'package:bigodon/Screens/Camera/preview_page.dart';
import 'package:bigodon/Screens/side_menu.dart';
import 'package:bigodon/components/login_input_field.dart';
import 'package:bigodon/components/rounded_button.dart';
import 'package:bigodon/controllers/usuario_controller.dart';
import 'package:bigodon/widgets/image_screen.dart';
import 'package:camera_camera/camera_camera.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class MyAccountPage extends StatefulWidget {
  MyAccountPage({Key key}) : super(key: key);

  @override
  _MyAccountPageState createState() => _MyAccountPageState();
}

class _MyAccountPageState extends State<MyAccountPage> {
  UsuarioController controller_usuario = Get.put(UsuarioController());
  String _email;
  String _senha;
  String _nome;
  File arquivo;

  final picker = ImagePicker();

  getFileFromGallery() async {
    var imagem = await picker.getImage(source: ImageSource.gallery);

    if (imagem != null) {
      // transição
      await controller_usuario.uploadFotoUsuario(File(imagem.path));
      setState(() {
        controller_usuario.carregarUsuario();
      });
      Get.back();
    }
  }

  @override
  void initState() {
    super.initState();
    _email = controller_usuario.usuario.email;
    _nome = controller_usuario.usuario.nome;
  }

  showPreview(imagem) async {
    await Get.to(() => PreviewPage(file: imagem));
    if (imagem != null) {
      // transição
      await controller_usuario.uploadFotoUsuario(imagem);
      setState(() {
        controller_usuario.carregarUsuario();
      });
      Get.back();
    }
  }

  Column buildBottomNavigationMenu() {
    return Column(
      children: <Widget>[
        ListTile(
          leading: Icon(Icons.photo_album),
          title: Text('Escolher foto da galeria'),
          onTap: () {
            getFileFromGallery();
            //Navigator.pop();
          },
        ),
        ListTile(
          leading: Icon(Icons.camera_alt),
          title: Text('Tirar foto'),
          onTap: () {
            Get.to(
              () => CameraCamera(onFile: (file) => showPreview(file)),
            );
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        shape: ContinuousRectangleBorder(
          borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(30),
            bottomRight: Radius.circular(30),
          ),
        ),
        title: Image.asset(
          "assets/images/bigodon_black.png",
          fit: BoxFit.contain,
          height: 40,
        ),
      ),
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.fromLTRB(0, 40, 0, 20),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              GestureDetector(
                child: Hero(
                  tag: "imageHero",
                  child: Container(
                    width: 170,
                    height: 170,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: NetworkImage(
                          controller_usuario.usuario.image,
                        ),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
                onTap: () {
                  showModalBottomSheet(
                      context: context,
                      builder: (context) {
                        return Container(
                          color: Color(0xFF737373),
                          height: 115,
                          child: Container(
                            child: buildBottomNavigationMenu(),
                            decoration: BoxDecoration(
                                color: Theme.of(context).canvasColor,
                                borderRadius: BorderRadius.only(
                                  topLeft: const Radius.circular(19),
                                  topRight: const Radius.circular(19),
                                )),
                          ),
                        );
                      });
                  /*
                  Navigator.push(context, MaterialPageRoute(builder: (_) {
                    return ImageScreen();
                  }
                  ));
                  */
                },
              ),
              Container(
                padding: EdgeInsets.only(top: 30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Editar Informações",
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    LoginInputField(
                      hintText: controller_usuario.usuario.nome,
                      onChanged: (value) {
                        _nome = value;
                      },
                    ),
                    LoginInputField(
                      hintText: controller_usuario.usuario.email,
                      onChanged: (value) {
                        _email = value;
                      },
                      icon: Icons.email,
                    ),
                    LoginInputField(
                      hintText: "Nova Senha",
                      onChanged: (value) {
                        _senha = value;
                      },
                      icon: Icons.lock,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    RoundedButton(
                      text: "Salvar",
                      press: () async {
                        if (await controller_usuario.updateUsuario(
                            _nome, _email, _senha)) {
                          showSnack("Atualizado com sucesso!", 'Sucesso!');
                          setState(() {
                            controller_usuario.carregarUsuario();
                          });
                        } else {
                          showSnack(
                              "Dados informados estão invalidos !", 'Error!');
                        }
                      },
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  showSnack(String titulo, String erro) {
    Get.snackbar(titulo, erro,
        backgroundColor: Colors.grey[900],
        colorText: Colors.white,
        snackPosition: SnackPosition.BOTTOM);
  }
}
