import 'package:flutter/material.dart';
import 'package:bigodon/constants.dart';

class Background extends StatelessWidget {
  final Widget child;

  const Background({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: double.infinity,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          // Positioned(
          //   bottom: 30,
          //   child: Text(
          //     "Não tem uma conta?\nInscreva-se",
          //     textAlign: TextAlign.center,
          //     style:
          //         TextStyle(color: repPurple, fontSize: 20, letterSpacing: -1),
          //   ),
          // ),
          child,
        ],
      ),
    );
  }
}
