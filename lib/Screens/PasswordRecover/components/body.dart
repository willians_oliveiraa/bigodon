import 'package:flutter/material.dart';
import 'package:bigodon/Screens/PasswordRecover/components/background.dart';
import 'package:bigodon/Screens/PasswordRecover/password_recover_screen.dart';
import 'package:bigodon/Screens/Singup/singup_screen.dart';
import 'package:bigodon/components/have_account_check.dart';
import 'package:bigodon/components/login_input_field.dart';
import 'package:bigodon/components/password_input_field.dart';
import 'package:bigodon/components/rounded_button.dart';
import 'package:bigodon/components/text_field_container.dart';
import 'package:bigodon/constants.dart';
import 'package:bigodon/widgets/float_logo.dart';

class Body extends StatelessWidget {
  const Body({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Positioned(
              top: 10,
              child: FloatLogo(
                image: "assets/images/bigodon_black.png",
                size: size,
              ),
            ),
            SizedBox(
              height: size.height * 0.05,
            ),
            Text(
              "Enviar nova senha",
              style: TextStyle(
                fontSize: 35,
                fontWeight: FontWeight.w700,
                color: Colors.black,
              ),
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            LoginInputField(
              hintText: "Seu E-mail",
              onChanged: (value) {},
            ),
         
            RoundedButton(
              text: "Enviar",
              press: () {},
              color: Colors.grey,
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            HaveAccountCheck(
              login: true,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return SingupScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
