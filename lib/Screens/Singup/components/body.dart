import 'package:bigodon/Screens/HomePage/home_page_screen.dart';
import 'package:bigodon/controllers/login_controller.dart';
import 'package:bigodon/models/user.dart';
import 'package:flutter/material.dart';
import 'package:bigodon/Screens/Login/login_screen.dart';
import 'package:bigodon/Screens/Singup/components/background.dart';
import 'package:bigodon/Screens/Singup/singup_screen.dart';
import 'package:bigodon/components/have_account_check.dart';
import 'package:bigodon/components/login_input_field.dart';
import 'package:bigodon/components/password_input_field.dart';
import 'package:bigodon/components/rounded_button.dart';
import 'package:bigodon/components/text_field_container.dart';
import 'package:bigodon/constants.dart';
import 'package:bigodon/widgets/float_logo.dart';

class Body extends StatefulWidget {
  const Body({
    Key key,
  }) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  LoginController loginController;
  String email;
  String nome;
  String senha;

  @override
  void initState() {
    super.initState();
    loginController = LoginController();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Positioned(
              top: 10,
              child: FloatLogo(
                image: "assets/images/bigodon_black.png",
                size: size,
              ),
            ),
            SizedBox(
              height: size.height * 0.05,
            ),
            Text(
              "Inscreva-se",
              style: TextStyle(
                fontSize: 35,
                fontWeight: FontWeight.w700,
                color: Colors.red,
              ),
            ),
            SizedBox(
              height: size.height * 0.05,
            ),
            LoginInputField(
              hintText: "Nome Completo",
              onChanged: (value) {
                nome = value;
              },
            ),
            LoginInputField(
              hintText: "Seu E-mail",
              onChanged: (value) {
                email = value;
              },
            ),
            PasswordInputField(
              onChanged: (value) {
                senha = value;
              },
            ),
            RoundedButton(
              text: "Cadastrar",
              press: () async {
                if ((email == null || email == ' ') && (senha == null)) {
                  return ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                          content: Text('Email ou senha vazio!! :/')));
                }
                if (await loginController.cadastrarUsuario(
                    usuario: User(nome, email, senha, 15, ''))) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return HomePage();
                      },
                    ),
                  );
                } else {
                  return ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text(
                          'Impossível cadastrar o usuario! Email já cadastrado!! :/')));
                }
              },
              color: Colors.grey,
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            HaveAccountCheck(
              login: false,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return LoginScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  bool validaEmail(String email) {}
}
