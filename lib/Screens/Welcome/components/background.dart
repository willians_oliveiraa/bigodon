import 'package:flutter/material.dart';
import 'package:bigodon/widgets/float_logo.dart';

class Background extends StatelessWidget {
  final Widget child;

  const Background({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: double.infinity,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned(
            top: 0,
            left: 0,
            child: Image.asset(
              "assets/images/welcome_bg.png",
              width: size.width * 1,
            ),
          ),
          Positioned(
            top: 200,
            child: FloatLogo(
              image: "assets/images/bigodon_white.png",
              size: size,
            ),
          ),
          Positioned(
            bottom: 20,
            child: Text(
              "E W Y",
              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20),
            ),
          ),
          child,
        ],
      ),
    );
  }
}
