import 'package:flutter/material.dart';
import 'package:bigodon/Screens/Login/login_screen.dart';
import 'package:bigodon/Screens/Singup/singup_screen.dart';
import 'package:bigodon/Screens/Welcome/components/background.dart';
import 'package:bigodon/components/rounded_button.dart';
import 'package:bigodon/constants.dart';

class Body extends StatelessWidget {
  const Body({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: size.height * 0.50,
            ),
            Text(
              "Bem vindo!",
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 35,
                color: Colors.black,
              ),
            ),
            SizedBox(
              height: size.height * 0.04,
            ),
            RoundedButton(
              text: "Login",
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return LoginScreen();
                    },
                  ),
                );
              },
              color: Colors.grey,
            ),
            RoundedButton(
              text: "Cadastre-se",
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return SingupScreen();
                    },
                  ),
                );
              },
              color: Colors.grey,
            )
          ],
        ),
      ),
    );
  }
}
