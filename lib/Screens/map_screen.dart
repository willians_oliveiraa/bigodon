import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import '../controllers/barbearia_controller.dart';

class MapScreen extends StatefulWidget {
  const MapScreen({Key key}) : super(key: key);

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  final controller = Get.put(BarbeariaController());

  @override
  Widget build(BuildContext context) {
    // transição
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        shape: ContinuousRectangleBorder(
          borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(30),
            bottomRight: Radius.circular(30),
          ),
        ),
        title: Image.asset(
          "assets/images/bigodon_black.png",
          fit: BoxFit.contain,
          height: 40,
        ),
        actions: [
          IconButton(
              icon: Icon(Icons.filter_list),
              onPressed: () {
                controller.getPosicao();
              })
        ],
      ),
      body: GetBuilder<BarbeariaController>(
          init: controller,
          builder: (value) => GoogleMap(
            mapType: MapType.normal,
            zoomControlsEnabled: true,
            initialCameraPosition: CameraPosition(
              target: controller.position,
              zoom: 13,
            ),
            onMapCreated: controller.onMapCreated,
            myLocationEnabled: true,
            markers: controller.markers,
          ),
        )
    );
  }
}
