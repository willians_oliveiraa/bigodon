import 'package:bigodon/Screens/Favorites/favorite_page.dart';
import 'package:bigodon/Screens/Login/login_screen.dart';
import 'package:bigodon/Screens/MyAccount/my_account_screen.dart';
import 'package:bigodon/controllers/usuario_controller.dart';
import 'package:bigodon/widgets/image_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SideMenu extends StatefulWidget {
  const SideMenu({Key key}) : super(key: key);

  @override
  _SideMenuState createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {
  final UsuarioController controller = Get.put(UsuarioController());

  @override
  void initState() {
    super.initState();
    
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            padding: EdgeInsets.fromLTRB(20, 50, 20, 10),
            color: Colors.black,
            child: Center(
              child: Column(
                children: <Widget>[
                  GestureDetector(
                    child: Hero(
                      tag: "imageHero",
                      child: Container(
                        width: 120,
                        height: 120,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            image: NetworkImage(controller.usuario.image),
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                    ),
                    onTap: () {
                      if(controller.usuario.image != null){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (_) {
                          return ImageScreen(
                              imageToFull: controller.usuario.image);
                        }),
                      );
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('Carregando')));
                    }
                    },
                  ),
                  Container(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: Text(
                        " Olá, ${controller.usuario.nome} !",
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      )),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(20, 50, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ListTile(
                  leading: Icon(
                    Icons.person,
                    size: 30,
                  ),
                  title: Text(
                    "Minha Conta",
                    style: TextStyle(fontSize: 22),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          return MyAccountPage();
                        },
                      ),
                    );
                  },
                ),
                ListTile(
                  leading: Icon(
                    Icons.calendar_today,
                    size: 30,
                  ),
                  title: Text(
                    "Minha Agenda",
                    style: TextStyle(fontSize: 22),
                  ),
                  onTap: () {
                    ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('Em breve! ;)')));
                  },
                ),
                ListTile(
                  leading: Icon(
                    Icons.favorite,
                    size: 30,
                    color: Colors.red[400],
                  ),
                  title: Text(
                    "Favoritos",
                    style: TextStyle(fontSize: 22),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (_) {
                        return FavoritePage();
                      }),
                    );
                  },
                ),
                ListTile(
                  leading: Icon(
                    Icons.history,
                    size: 30,
                  ),
                  title: Text(
                    "Histórico",
                    style: TextStyle(fontSize: 22),
                  ),
                  onTap: () {
                    ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('Em breve! ;)')));
                  },
                ),
                ListTile(
                  leading: Icon(
                    Icons.power_settings_new,
                    size: 30,
                    color: Colors.red,
                  ),
                  title: Text(
                    "Sair",
                    style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.w700,
                        color: Colors.red),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          return LoginScreen();
                        },
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
