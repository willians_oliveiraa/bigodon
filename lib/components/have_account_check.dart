import 'package:flutter/material.dart';
import 'package:bigodon/constants.dart';

class HaveAccountCheck extends StatelessWidget {
  final bool login;
  final Function press;
  const HaveAccountCheck({
    Key key,
    this.login = true,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          login ? "Não tem uma conta? " : "Já tem uma conta? ",
          style: TextStyle(color: Colors.black, fontSize: 17),
        ),
        GestureDetector(
          onTap: () {},
          child: Text(
            login ? "Inscreva-se" : "Faça login",
            style: TextStyle(
              color: Colors.red,
              fontSize: 17,
              fontWeight: FontWeight.w500,
            ),
          ),
        )
      ],
    );
  }
}
