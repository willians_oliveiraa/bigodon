import 'package:flutter/material.dart';
import 'package:bigodon/components/text_field_container.dart';
import 'package:bigodon/constants.dart';

class LoginInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChanged;
  const LoginInputField({
    Key key,
    this.hintText,
    this.icon = Icons.person,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        onChanged: onChanged,
        decoration: InputDecoration(
            prefixIcon: Icon(
              icon,
              size: 30,
              color: Colors.grey,
            ),
            hintText: hintText,
            hintStyle: TextStyle(fontSize: 20)),
      ),
    );
  }
}
