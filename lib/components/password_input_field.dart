import 'package:flutter/material.dart';
import 'package:bigodon/components/text_field_container.dart';
import 'package:bigodon/constants.dart';

class PasswordInputField extends StatefulWidget {
  final ValueChanged<String> onChanged;

  const PasswordInputField({
    Key key,
    this.onChanged,
  }) : super(key: key);

  @override
  _PasswordInputFieldState createState() => _PasswordInputFieldState();
}

class _PasswordInputFieldState extends State<PasswordInputField> {

  bool obscureText = true;

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        obscureText: obscureText,
        onChanged: widget.onChanged,
        decoration: InputDecoration(
          hintText: "Senha",
          hintStyle: TextStyle(fontSize: 20, letterSpacing: 0),
          prefixIcon: Icon(
            Icons.lock,
            color: Colors.grey,
          ),
          suffixIcon: InkWell(
            onTap: _toggleObscureText,
            child: Icon(
              Icons.visibility,
              color: Colors.grey,
            ),
          ),
        ),
      ),
    );
  }
  void _toggleObscureText(){
    obscureText = !obscureText;
    setState(() {
          
        });
  }
}