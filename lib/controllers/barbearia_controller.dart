import 'package:bigodon/models/barbearia.dart';
import 'package:bigodon/repositories/barbearia_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class BarbeariaController extends GetxController {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  BarbeariaRepository barbeariaRepository;
  RxList<dynamic> barbearias = [].obs;

  get tabela => barbearias;
  // GOOGLE MAPS
  GoogleMapController _mapsController;
  LatLng _position = LatLng(-25.0881083, -50.159783);
  final latitude = 0.0.obs;
  final longitude = 0.0.obs;
  final markers = Set<Marker>().obs;

  static BarbeariaController get to => Get.find<BarbeariaController>();
  get mapsController => _mapsController;
  get position => _position;

  BarbeariaController() {
    carregarBarbearias();
  }

  carregarBarbearias() async {
    await firestore.collection('barbearias').get().then((querySnapshot) async {
      querySnapshot.docs.forEach((document) async {
        List<String> galeria_url = [];
        List<String> horarios = [];

        await firestore
            .collection('barbearia_galeria')
            .where('id_barbearia', isEqualTo: document.id)
            .get()
            .then((queryBarbearia) async {
          queryBarbearia.docs.forEach((imagem) {
            galeria_url.add(imagem.data()['url_imagem']);
          });
        });

        await firestore
            .collection('barbearia_horario')
            .where('id_barbearia', isEqualTo: document.id)
            // .orderBy('hora_ini')
            .get()
            .then((queryHorario) async {
          queryHorario.docs.forEach((horario) {
            horarios.add(horario.data()['hora_ini']);
          });
        });

        barbearias.add(Barbearia(
            descricao: document.data()['descricao'],
            galeria: galeria_url,
            id: document.id,
            horarios: horarios,
            logo: document.data()['logo'],
            distancia: document.data()['distancia'],
            imagemFundo: document.data()['img_fundo'],
            like: true,
            nome: document.data()['nome'],
            telefone: document.data()['telefone'],
            local: document.data()['local']));
      });
    });
  }

  carregarMarkes() {
    if (markers.length == 0) {
      if (barbearias.length > 0) {
        print(barbearias.length);
        barbearias.forEach((barbearia) {
          markers.add(
            Marker(
                markerId: MarkerId(barbearia.id),
                position:
                    LatLng(barbearia.local.latitude, barbearia.local.longitude),
                infoWindow: InfoWindow(title: barbearia.nome),
                onTap: () => {}),
          );
        });
      }
    }
  }

  onMapCreated(GoogleMapController gmc) async {
    _mapsController = gmc;
    carregarMarkes();
    getPosicao();
    //loadBarbers
  }

  loadBarbers() async {}

  Future<Position> _posicaoatual() async {
    LocationPermission permissao;
    bool ativado = await Geolocator.isLocationServiceEnabled();

    if (!ativado) {
      return Future.error('Por favor, habilite a localização do smartphone');
    }

    permissao = await Geolocator.checkPermission();
    if (permissao == LocationPermission.denied) {
      permissao = await Geolocator.requestPermission();

      if (permissao == LocationPermission.denied) {
        return Future.error('Você precisa autorizar o acesso à localização');
      }
    }

    if (permissao == LocationPermission.deniedForever) {
      return Future.error('Autorize o acesso à localização nas configurações');
    }

    return await Geolocator.getCurrentPosition();
  }

  getPosicao() async {
    try {
      final posicao = await _posicaoatual();
      latitude.value = posicao.latitude;
      longitude.value = posicao.longitude;
      _mapsController.animateCamera(
        CameraUpdate.newLatLng(
          LatLng(latitude.value, longitude.value),
        ),
      );
    } catch (e) {
      Get.snackbar(
        'Erro',
        e.toString(),
        backgroundColor: Colors.grey[900],
        colorText: Colors.white,
        snackPosition: SnackPosition.BOTTOM,
      );
    }
  }
}
