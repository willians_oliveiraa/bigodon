import 'package:bigodon/models/barbearia.dart';
import 'package:bigodon/repositories/barbearia_repository.dart';

class HomeController {
  BarbeariaRepository barbeariaRepository;

  List<Barbearia> get tabela => barbeariaRepository.barbearias;

  HomeController() {
    barbeariaRepository = BarbeariaRepository();
  }
}
