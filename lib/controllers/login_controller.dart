import 'package:bigodon/repositories/user_repository.dart';
import 'package:bigodon/models/user.dart';
import 'package:bigodon/services/auth_service.dart';

class LoginController {
  AuthService user;

  // List<User> get usuarios => userRepository.users || List();

  LoginController() {
    this.user = AuthService();
  }

  Future<bool> login({String email, String password}) async {
    await AuthService.to.login(email, password);
    return AuthService.to.userIsAuthenticated.value;
  }

  Future<bool> cadastrar(
      {String email, String password, String nome, int idade}) async {
    await AuthService.to.createUser(email, password, nome, idade);
    return AuthService.to.userIsAuthenticated.value;
  }

  logout() async {
    AuthService.to.logout();
  }

  userIsLogado() {
    return AuthService.to.userIsAuthenticated.value;
  }

  // User validaUsuario({email, senha}) {
  //   for (var i = 0; i < usuarios.length; i++) {
  //     if (usuarios[i].email == email) {
  //       if (usuarios[i].senha == senha) {
  //         return usuarios[i];
  //       }
  //     }
  //   }
  //   return null;
  // }

  Future<bool> cadastrarUsuario({User usuario}) async {
    return await AuthService.to
        .createUser(usuario.email, usuario.senha, usuario.nome, usuario.idade);
  }

  getIdUsuario() {
    return AuthService.to.user.uid;
  }
}
