// import 'package:bigodon/controllers/login_controller.dart';
import 'dart:io';
import 'package:bigodon/controllers/login_controller.dart';
import 'package:bigodon/controllers/my_controller.dart';
import 'package:bigodon/models/user.dart';
import 'package:bigodon/services/auth_service.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';

class UsuarioController extends My_Controller {
  var usuario;
  var storage;
  var hashUser;

  UsuarioController() {
    hashUser = LoginController().getIdUsuario();
    storage = FirebaseStorage.instance;
    carregarUsuario();
  }

  carregarUsuario() async {
    await firestore
        .collection('usuario')
        .where('id_usuario', isEqualTo: hashUser)
        .get()
        .then((querySnapshot) async {
      var document = querySnapshot.docs.first.data();
      usuario = User(document['nome'], AuthService.to.user.email, '',
          document['idade'], document['image']);
      print(Get.find().nome);
    });
  }

  Future<bool> updateUsuario(nome, email, senha) async {
    var validation = false;
    await firestore
        .collection('usuario')
        .where('id_usuario', isEqualTo: AuthService.to.user.uid)
        .get()
        .then((querySnapshot) async {
      var update = querySnapshot.docs.first.id;
      if (email != null) {
        validation = true;
        await AuthService.to.updateEmail(email);
      }
      if (senha != null && senha.length > 6) {
        validation = true;
        await AuthService.to.updateSenha(senha);
      }
      if (nome != null) {
        validation = true;
        firestore.collection('usuario').doc(update).update({'nome': nome});
      }
    });
    return validation;
  }

  uploadFotoUsuario(File image) async {
    Reference ref =
        storage.ref().child('imagens_perfil').child('/${hashUser}.jpg');
    await ref.putFile(File(image.path), null);

    await storage
        .ref('imagens_perfil/${hashUser}.jpg')
        .getDownloadURL()
        .then((cloud) async {
      await firestore
          .collection('usuario')
          .where('id_usuario', isEqualTo: hashUser)
          .get()
          .then((querySnapshot) async {
        var update = querySnapshot.docs.first.id;
        firestore.collection('usuario').doc(update).update({'image': cloud});
      });
    });
  }
}
