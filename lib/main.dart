import 'package:bigodon/services/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:bigodon/Screens/Welcome/welcome_screen.dart';
import 'package:bigodon/constants.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get.dart';

// oi
//aaaaaaaaaaaaaaaa
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  connectFirebase();
  Get.lazyPut<AuthService>(() => AuthService());
  runApp(MyApp());
}

Future<void> connectFirebase() async {
  await Firebase.initializeApp();
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Bigodon',
      theme: ThemeData(
        fontFamily: 'Human Regular',
        primaryColor: Colors.white,
        scaffoldBackgroundColor: Colors.white,
      ),
      home: WelcomeScreen(),
    );
  }
}
