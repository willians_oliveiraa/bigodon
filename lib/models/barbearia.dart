import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Barbearia {
  String id;
  String nome;
  String distancia;
  String descricao;
  String logo;
  String imagemFundo;
  String telefone;
  bool like;
  List<String> horarios;
  List<String> galeria;
  GeoPoint local;

  void setLike(like) {
    this.like = !like;
  }

  Barbearia(
      {this.id,
      this.nome,
      this.distancia,
      this.descricao,
      this.logo,
      this.like,
      this.imagemFundo,
      this.telefone,
      this.horarios,
      this.galeria,
      this.local});
}
