import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  FirebaseFirestore firestore = FirebaseFirestore.instance;

  int _idade;
  String _nome;
  String _senha;
  String _email;
  String _image;
  String _latitude;
  String _longitude;

  User(this._nome, this._email, this._senha, this._idade, this._image);

  get latitude => this._latitude;
  get longtude => this._longitude;

  set email(email) => this._email = email;
  get email => this._email;

  set nome(nome) => this._nome = nome;
  get nome => this._nome;

  set senha(senha) => this._senha = senha;
  get senha => this._senha;

  set idade(idade) => this._idade = idade;
  get idade => this._idade;

  set image(image) => this._image = image;
  get image => this._image;

  void setLatitude({latitude}) {
    this._latitude = latitude;
  }

  void setLongitude({longitude}) {
    this._longitude = longitude;
  }

  createUser(hashUser) async {
    await firestore.collection('usuario').add({
      'id_usuario': hashUser,
      'idade': idade,
      'image':
          'https://firebasestorage.googleapis.com/v0/b/bigodon-1f623.appspot.com/o/614743.png?alt=media&token=c4e3a1cc-1cc4-422d-b6cb-46e0353caecf',
      'nome': nome
    });
  }
}
