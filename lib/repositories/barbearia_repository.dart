import 'dart:collection';

import 'package:bigodon/models/barbearia.dart';
import 'package:flutter/material.dart';
//import '../models/barbearia.dart';

class BarbeariaRepository {
  final List<Barbearia> _barbearias = [];

  get barbearias => this._barbearias;

  // UnmodifiableListView<Barbearia> get barbearias =>
  //     UnmodifiableListView(_barbearias);

  BarbeariaRepository() {
    _barbearias.addAll([
      Barbearia(
          id: '1',
          nome: "Prondos",
          descricao: "Endereço Exemplo, 1234, Centro",
          distancia: "1.1 Km",
          logo: "assets/images/barber_example.png",
          like: true,
          imagemFundo: "assets/images/reforma-de-barbearia.jpg",
          telefone: "(11) 9999-9999",
          horarios: [
            "12:00",
            "13:00",
            "14:00",
            "15:00",
            "16:00",
            "17:00",
            "18:00"
          ],
          galeria: [
            "assets/images/barbearia3.jpeg",
            "assets/images/barbearia2.jpg",
            "assets/images/reforma-de-barbearia.jpg",
            "assets/images/barbearia3.jpeg",
            "assets/images/barbearia2.jpg"
          ]),
      Barbearia(
          id: '2',
          nome: "Predrosos Cabelereiro",
          descricao: "Endereço Exemplo, 1234, Centro",
          distancia: "4.1 Km",
          logo: "assets/images/barber_example2.png",
          like: false,
          imagemFundo: "assets/images/barbearia3.jpeg",
          telefone: "(11) 9999-9999",
          horarios: [
            "12:00",
            "13:00",
            "14:00",
            "15:00",
            "16:00",
            "17:00",
            "18:00"
          ],
          galeria: [
            "assets/images/barbearia3.jpeg",
            "assets/images/barbearia2.jpg",
            "assets/images/reforma-de-barbearia.jpg"
          ]),
      Barbearia(
          id: '3',
          nome: "Cleber Barber",
          descricao: "Endereço Exemplo, 1234, Centro",
          distancia: "2.3 Km",
          logo: "assets/images/barber_example3.png",
          like: true,
          imagemFundo: "assets/images/barbearia2.jpg",
          telefone: "(11) 9999-9999",
          horarios: [
            "12:00",
            "13:00",
            "14:00",
            "15:00",
            "16:00",
            "17:00",
            "18:00"
          ],
          galeria: [
            "assets/images/barbearia3.jpeg",
            "assets/images/barbearia2.jpg",
            "assets/images/reforma-de-barbearia.jpg"
          ]),
      Barbearia(
          id: '4',
          nome: "Tete",
          descricao: "Endereço Exemplo, 1234, Centro",
          distancia: "99.1 Km",
          logo: "assets/images/barber_example.png",
          like: false,
          imagemFundo: "assets/images/reforma-de-barbearia.jpg",
          telefone: "(11) 9999-9999",
          horarios: [
            "12:00",
            "13:00",
            "14:00",
            "15:00",
            "16:00",
            "17:00",
            "18:00"
          ],
          galeria: [
            "assets/images/barbearia3.jpeg",
            "assets/images/barbearia2.jpg",
            "assets/images/reforma-de-barbearia.jpg"
          ]),
      Barbearia(
          id: '5',
          nome: "Asdasd",
          descricao: "Endereço Exemplo, 1234, Centro",
          distancia: "99.1 Km",
          logo: "assets/images/barber_example2.png",
          like: false,
          imagemFundo: "assets/images/reforma-de-barbearia.jpg",
          telefone: "(11) 9999-9999",
          horarios: [
            "12:00",
            "13:00",
            "14:00",
            "15:00",
            "16:00",
            "17:00",
            "18:00"
          ],
          galeria: [
            "assets/images/barbearia3.jpeg",
            "assets/images/barbearia2.jpg",
            "assets/images/reforma-de-barbearia.jpg"
          ]),
    ]);
  }
}
