import 'dart:collection';

import 'package:bigodon/models/user.dart';

class UserRepository {
  final List<User> _users = [];
  static final UserRepository _userRepository = UserRepository._internal();

  factory UserRepository() {
    return _userRepository;
  }

  UserRepository._internal() {
    _users.addAll([
      User('root', 'root', 'root', 15, ''),
      User('Yuri', 'yuri@utf.com', 'yuri', 15, ''),
      User('Willians', 'will@utf.com', 'will', 15, ''),
      User('Eric', 'eric@utf.com', 'eric', 15, ''),
    ]);
  }

  get users => this._users;

  bool addUsuario({User usuario}) {
    int tamanho = _users.length;
    _users.add(usuario);
    if (tamanho < _users.length) return true;
    return false;
  }
}
