import 'package:bigodon/models/user.dart' as usuario;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AuthService extends GetxController {
  FirebaseAuth _auth = FirebaseAuth.instance;
  Rx<User> _firabaseUser;
  var userIsAuthenticated = false.obs;

  @override
  void onInit() {
    super.onInit();

    _firabaseUser = Rx<User>(_auth.currentUser);
    _firabaseUser.bindStream(_auth.authStateChanges());

    ever(_firabaseUser, (User user) {
      if (user != null) {
        userIsAuthenticated.value = true;
      } else {
        userIsAuthenticated.value = false;
      }
    });
  }

  User get user => _firabaseUser.value;
  static AuthService get to => Get.find<AuthService>();

  showSnack(String titulo, String erro) {
    Get.snackbar(titulo, erro,
        backgroundColor: Colors.grey[900],
        colorText: Colors.white,
        snackPosition: SnackPosition.BOTTOM);
  }

  createUser(String email, String password, String nome, int idade) async {
    await logout();
    try {
      await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      await login(email, password);
      await usuario.User(nome, email, password, idade, '')
          .createUser(_auth.currentUser.uid);
    } catch (e) {
      showSnack("Erro ao registrar!", e.message);
    }
    return true;
  }

  login(String email, String password) async {
    try {
      await _auth.signInWithEmailAndPassword(email: email, password: password);
    } catch (e) {
      showSnack("Erro no login", e.message);
    }
  }

  logout() async {
    try {
      await _auth.signOut();
    } catch (e) {
      showSnack("Erro no login", e.message);
    }
  }

  updateEmail(String email) async {
    try {
      await _auth.currentUser.updateEmail(email);
    } catch (e) {
      showSnack("Erro ao atualizar seu perfil !", e.message);
    }
  }

  updateSenha(String senha) async {
    try {
      await _auth.currentUser.updatePassword(senha);
    } catch (e) {
      showSnack("Senha precisa de no minímo 6 caracteres !", e.message);
    }
  }
}
