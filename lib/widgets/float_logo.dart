import 'package:flutter/material.dart';

class FloatLogo extends StatelessWidget {
  final String image;
  final Size size;

  const FloatLogo({
    Key key,
    this.image, this.size,
  }) : super(key: key);

  Widget build(BuildContext context) {
    return SizedBox(
      child: Hero(
        tag: image,
        child: Image.asset(
          image,
          width: size.width * 0.5,
        ),
      ),
    );
  }
}
