import 'package:flutter/material.dart';

class ImageScreen extends StatelessWidget {
  final String imageToFull;

  const ImageScreen({Key key, this.imageToFull}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        shape: ContinuousRectangleBorder(
          borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(30),
            bottomRight: Radius.circular(30),
          ),
        ),
        title: Image.asset(
          "assets/images/bigodon_black.png",
          fit: BoxFit.contain,
          height: 40,
        ),
      ),
      body: GestureDetector(
        child: Center(
          child: Hero(
            tag: 'imageHero',
            child: 
            imageToFull != null ?
              Image.network(imageToFull,)
              : 
              Image.asset('assets/images/usuario.jpg',),
          ),
        ),
        onTap: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}

